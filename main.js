const textInput = document.getElementById('text');
const content = document.getElementById('content');

// untuk menambahkan data user
function add() {
    const inner = document.createElement('p');
    const btn = document.createElement('i');

        // untuk menambahkan angka sesuai panjang data
        inner.textContent = `${content.children.length + 1}. ${textInput.value}`;
        // ===

        inner.setAttribute('id', content.children.length);
        inner.setAttribute('class', 'box');

        // fungsi menghapus sesuai id parent
        btn.setAttribute('onclick', 'removedId(this.parentNode.id)');
        btn.setAttribute('class', 'fa-solid fa-trash')

        // menampilkan item/content
        inner.append(btn);
        content.append(inner);

        // membuat text input jadi kosong setelah tidak focus
        textInput.value = '';
}

// untuk hapus 1 dari bawah
function remove() {
    content.lastChild.remove();
}

// untuk hapus sesuai current
function removedId(id) {
    const element = document.getElementById(id)
    element.remove()
}
